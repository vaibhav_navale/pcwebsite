package com.email;

import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailFromFeedback {
	 public  void sendEmail(int star1,int  star2, int star3,int star4,String comments){
		  
		    final String username = "vaibhavnavale.tcs@gmail.com";
	        final String password = "Vaibhav@1";
	      
	        Properties props = new Properties();
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.auth", "true");
	        props.put("mail.smtp.host", "smtp.gmail.com");
	        props.put("mail.smtp.port", "587");

	        Session session = Session.getInstance(props,
	          new javax.mail.Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication(username, password);
	            }
	          });

	        try {

	            Message message = new MimeMessage(session);
	            message.setFrom(new InternetAddress(username));
	            message.setRecipients(Message.RecipientType.TO,
	                InternetAddress.parse("9le.vaibhav@gmail.com"));
	            message.setSubject("Feedback");
	            message.setSentDate(new Date());
	            message.setText("From Feedback Section "+"\n"+"\n"+"How would you rate the search feature?:\n"+"Rating:"+star1+" "
	            +"Star"+"\n"+"How would you rate the filter feature?:\n"+"Rating:"+star2+" "
	            		+"Star"+"\n"+"How would you rate the print feature?:\n"+"Rating:"+star3+" "
	            +"Star"+"\n"+"How would you rate your overall satisfaction this application?:\n"+"Rating:"+star4+" "
	            		+"Star"+"\n"+"Feedback: "+"\n"+comments);
         
	            
	            Transport.send(message);

	            System.out.println("Done");

	        } catch (MessagingException e) {
	            throw new RuntimeException(e);
	        }
	
	    }
}
