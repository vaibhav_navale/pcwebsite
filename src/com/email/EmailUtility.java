package com.email;

import java.util.Date;
import java.util.Properties;
 
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
 
/**
 * A utility class for sending e-mail messages
 * @author www.codejava.net
 *
 */
public class EmailUtility {
	
	public void sendEmail(String sender_emailid,String subject, String sender_enquiry_message,String sender_username,String mobile){
		System.out.println("in email method");

		 final String username = "vaibhavnavale.tcs@gmail.com";
	        final String password = "Vaibhav@1";
	       	        
	        Properties props = new Properties();
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.smtp.auth", "true");
	        props.put("mail.smtp.host", "smtp.gmail.com");
	        props.put("mail.smtp.port", "587");

	        Session session = Session.getInstance(props,
	          new javax.mail.Authenticator() {
	            protected PasswordAuthentication getPasswordAuthentication() {
	                return new PasswordAuthentication(username, password);
	            }
	          });

	        try {

	            Message message = new MimeMessage(session);
	            message.setFrom(new InternetAddress(username));
	            message.setRecipients(Message.RecipientType.TO,
	                InternetAddress.parse("9le.vaibhav@gmail.com"));
	            message.setSubject(subject);
	            message.setSentDate(new Date());
	            message.setText("\n\n\t Name : "+sender_username+"\n"+
	            		         "\t Mobile Number : "+mobile+"\n"+
	            		         "\t User Enquiry Message : \n"+"\t"+sender_enquiry_message+"\n"
	                + "\n\n\t E-mail Id : "+sender_emailid);
            
	            
	            Transport.send(message);

	            System.out.println("Done");

	        } catch (MessagingException e) {
	            throw new RuntimeException(e);
	        }
	
	}

}