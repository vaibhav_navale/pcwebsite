package com.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.DbOperation;
import com.email.EmailFromFeedback;
import com.email.EmailUtility;

/**
 * Servlet implementation class Feedback
 */
public class Feedback extends HttpServlet {
	private static final long serialVersionUID = 1L;
	 private String host;
	   private String port;
	   private String user;
	   private String pass;
	
	public void init() {
		  
		 
	        // reads SMTP server setting from web.xml file
	        ServletContext context = getServletContext();
	        host = context.getInitParameter("host");
	        port = context.getInitParameter("port");
	        user = context.getInitParameter("user");
	        System.out.println("user address:"+user);
	        pass = context.getInitParameter("pass");
	    }
	 
  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		
		int star4=Integer.parseInt(request.getParameter("starrat4"));
		int star1=Integer.parseInt(request.getParameter("starrat1"));
		int star2=Integer.parseInt(request.getParameter("starrat2"));
		int star3=Integer.parseInt(request.getParameter("starrat3"));		
		String comment2=request.getParameter("comment");
		
		System.out.println("value of star"+star4);
		System.out.println("value of star"+star1);
		System.out.println("value of star"+star2);
		System.out.println("value of star"+star3);
		System.out.println("value of comment"+comment2);

		System.out.println(" message 1");
		DbOperation db=new DbOperation();
		System.out.println(" message 2");
		String returnvalue=db.insertfeedback(star4, star1,star2,star3,comment2,request);
		System.out.println(" message 3");
		System.out.println(returnvalue +" message in Contacts");		
        EmailFromFeedback emailFromFeedback=new EmailFromFeedback();
        emailFromFeedback.sendEmail(star1, star2,star3,star4,comment2);
        System.out.println("email send success");
				
	    }
		
	
	}


