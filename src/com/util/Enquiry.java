package com.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.db.DbOperation;
import com.email.EmailUtility;
import com.email.Emailcontact;

/**
 * Servlet implementation class Enquiry
 */
public class Enquiry extends HttpServlet {
	private static final long serialVersionUID = 1L;
	   private String host;
	   private String port;
	   private String user;
	   private String pass;
	
	public void init() {
		  
		 
	        // reads SMTP server setting from web.xml file
	        ServletContext context = getServletContext();
	        host = context.getInitParameter("host");
	        port = context.getInitParameter("port");
	        user = context.getInitParameter("user");
	        System.out.println("user address:"+user);
	        pass = context.getInitParameter("pass");
	    }
	 
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out=response.getWriter();
		response.setContentType("text/html");
		String username=request.getParameter("username");
		String emailid=request.getParameter("emailid");
		String mob=request.getParameter("mob");
		String sub=request.getParameter("sub");
		String enquiry=request.getParameter("enquiry");
		
		System.out.println("value of firstname:"+username);
		System.out.println("value of emailid:"+emailid);
		System.out.println("value of mob:"+mob);
		System.out.println("value of sub:"+sub);
		System.out.println("value of enquiry:"+enquiry);
		
		 System.out.println(" message 1");
		 DbOperation db=new DbOperation();
		 System.out.println(" message 2");
		 String returnvalue=db.InsertEnquiry(username, emailid,mob,sub,enquiry);
		 System.out.println(" message 3");
	     System.out.println(returnvalue +" message in Contacts");
	     EmailUtility emailUtility=new EmailUtility();
	     emailUtility.sendEmail(emailid, sub,enquiry,username,mob);
	     System.out.println("email send success");
     
	    
	    }
		
		
	

}
