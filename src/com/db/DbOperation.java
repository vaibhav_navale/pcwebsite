package com.db;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.omg.CORBA.portable.InputStream;

import com.model.User;


public class DbOperation {

	public Connection getConnectionwithdb(){
		Connection con=null;
		try{  
			Class.forName("com.mysql.jdbc.Driver");  
		   //con=DriverManager.getConnection("jdbc:mysql://localhost:3306/db756416182?useSSL=false","dbo756416182","PCRounders1!");
			con=DriverManager.getConnection("jdbc:mysql://localhost:3308/Pcrounders?useSSL=false","root","root");
		   
		}catch(Exception e){ System.out.println(e);}{
			
		}
		 return con;
	}  
	public String insert(User user){
		String message = "";
		try{
	    
		DbOperation db=new DbOperation();
		Connection con=db.getConnectionwithdb();
		PreparedStatement stmt=con.prepareStatement("insert into userregistration values(?,?,?,?,?)");  
		stmt.setString(1, user.getFirstname());
		stmt.setString(2, user.getLastname());
		stmt.setString(3, user.getEmail());
		stmt.setString(4, user.getPassword());
		stmt.setString(5, user.getConfirmpass());
		int row=stmt.executeUpdate();
		if(row>0){
			message= "success";
		}
		else{
			message= "failed";
		}
	}
		catch(Exception e)
		{ 
			message= "failed";
			System.out.println(e);
	    } 
		return message;
	}
	public String select(String email,String password){
		String message="";
		DbOperation db=new DbOperation();
		Connection con=db.getConnectionwithdb();
	    PreparedStatement stmt = null;
	    
	    try
	    {
	   
	    	stmt = con.prepareStatement("select email,password from userregistration where email=? AND password=?");
	    	stmt.setString(1, email);
	    	stmt.setString(2, password);
	    	ResultSet rs=stmt.executeQuery();
         if(rs.next()) 
          {
        	 message="SUCCESS";
	      }
         else
         {
        	 message="FAILED"; 
         }
	    }
	    catch(Exception e)
	    {
	    	message="FAILED"; 
	    	System.out.println(e);
	    }
	    return message;
	}
	public String insertfeedback(int star4,int star1,int star2,int star3,String comment2,HttpServletRequest request){
		String message = "";
		Timestamp time = null;
		String ip="";
		String ipAddress="";

		try
		{
	      System.out.println("In Insert Feedback Function");
		  DbOperation db=new DbOperation();
		  Connection con=db.getConnectionwithdb();
		  ipAddress = request.getRemoteAddr();  
	   	  System.out.print("ip address:"+ipAddress);
		
        String query1="select ipaddress from feedback1 where ipaddress=?";
		
		PreparedStatement st1 = con.prepareStatement(query1);
		//st1.setString(1, "192.168.50.88");
		st1.setString(1, ipAddress);
		ResultSet row=st1.executeQuery();
		if(row.next())
		{
			 String ipa=row.getString(1);
			 System.out.println("This is database retrived ip "+ipa);
			 String query2="select time from feedback1 where ipaddress=? order by time DESC limit 1";
			 PreparedStatement st2 = con.prepareStatement(query2);
			 
			 //st2.setString(1, "192.168.50.88");
			 st2.setString(1, ipAddress);
			 ResultSet r=st2.executeQuery();
				if(r.next())
				{
					time=r.getTimestamp(1);
					System.out.println(time);
					
				}
				
				Calendar calendar3 = Calendar.getInstance();
			    calendar3.setTime(time);
			    System.out.println();
			    System.out.println("This is the database time when we insert the record at first time");
			    System.out.println("Complete Date: "+time);
			    int year1=calendar3.get(Calendar.YEAR);
			    int month1=calendar3.get(Calendar.MONTH);
			    int date1=calendar3.get(Calendar.DAY_OF_MONTH);
			    int hours1 = calendar3.get(Calendar.HOUR_OF_DAY);
			    int minutes1 =calendar3.get(Calendar.MINUTE);
			    int seconds1 =calendar3.get(Calendar.SECOND);
			    calendar3.set(year1, month1, date1);
			    long miliSecondForDate1 = calendar3.getTimeInMillis();
			    
			    System.out.println("date1"+date1);
			    System.out.println("hours1"+hours1);
			    System.out.println("minutes1"+minutes1);
			    System.out.println("seconds1"+seconds1);
			    
			    
			    Timestamp timestamp = new Timestamp(System.currentTimeMillis());
			    Calendar calendar4 = Calendar.getInstance();
			    calendar4.setTime(timestamp);
			    int year=calendar4.get(Calendar.YEAR);
			    int month=calendar4.get(Calendar.MONTH);
			    int date=calendar4.get(Calendar.DAY_OF_MONTH);
			    int hours = calendar4.get(Calendar.HOUR_OF_DAY);
			    int minutes = calendar4.get(Calendar.MINUTE);
			    int seconds = calendar4.get(Calendar.SECOND);
			    
			    calendar4.set(year, month, date);
			   
			    long miliSecondForDate2 = calendar4.getTimeInMillis();
			    System.out.println("date"+date);
			    System.out.println("hours"+hours);
			    System.out.println("minutes"+minutes);
			    System.out.println("seconds"+seconds);
			   /* if(date==date1){
			    	    if(hours==hours1){
			    		*/
			    	
			    	long diff=miliSecondForDate2 - miliSecondForDate1;
			    	long diffMinutes = diff / (60 * 1000);
			    	System.out.println("Diffrence In Minutes "+diffMinutes);
			    		if(diffMinutes<1)
			    		{
			    		message="Fail";	
			    		}
			    		else
			    		{
			    			String sql1="insert into feedback1 (ipaddress,comment,time) values (?,?,?)"; 
			    			PreparedStatement st3 = con.prepareStatement(sql1, Statement.RETURN_GENERATED_KEYS);
			    			//st3.setString(1, "192.168.50.88");
			    			st3.setString(1, ipAddress);
			    			st3.setString(2, comment2);
			    			Timestamp timestamp1 = new Timestamp(System.currentTimeMillis());
			    			st3.setTimestamp(3,timestamp1);
			    			int row1=st3.executeUpdate();
			    			int lastid = 0;
			    			 ResultSet rs2 = st3.getGeneratedKeys();
			    			  
			    			    if (rs2.next())
			    			    {
			    			        lastid=rs2.getInt(1);
			    			        System.out.println("id"+lastid);
			    			    }
			    			    
			    			   
			    			    String sql3="insert into feedback_detail (questions,star_rating,feedno) values (?,?,?)"; 
			    				PreparedStatement stmt3 = con.prepareStatement(sql3, Statement.RETURN_GENERATED_KEYS);
			    				
			    			
			    				stmt3.setString(1, "How would you rate the search feature?");
			    				stmt3.setInt(2, star1);
			    				stmt3.setInt(3, lastid);
			    			
			    			    int row3=stmt3.executeUpdate();
			    			    String sql4="insert into feedback_detail (questions,star_rating,feedno) values (?,?,?)"; 
			    				PreparedStatement stmt4 = con.prepareStatement(sql4, Statement.RETURN_GENERATED_KEYS);
			    				
			    			
			    				stmt4.setString(1, "How would you rate the filter feature?");
			    				stmt4.setInt(2, star2);
			    				stmt4.setInt(3, lastid);
			    				int row4=stmt4.executeUpdate();
			    			    
			    		        String sql5="insert into feedback_detail (questions,star_rating,feedno) values (?,?,?)"; 
			    				PreparedStatement stmt5 = con.prepareStatement(sql5, Statement.RETURN_GENERATED_KEYS);
			    				stmt5.setString(1, "How would you rate the print feature?");
			    				stmt5.setInt(2, star3);
			    				stmt5.setInt(3, lastid);
			    				int row5=stmt5.executeUpdate();
			    				
			    				String sql6="insert into feedback_detail (questions,star_rating,feedno) values (?,?,?)"; 
			    				PreparedStatement stmt6 = con.prepareStatement(sql6, Statement.RETURN_GENERATED_KEYS);
			    				stmt6.setString(1, "How would you rate your overall satisfaction this application?");
			    				stmt6.setInt(2, star4);
			    				stmt6.setInt(3, lastid);
			    				int row6=stmt6.executeUpdate();
			    				
			    				
			    				
			    				if((row3>0 && row4>0)&& (row5>0 && row6>0))
			    				{
			    					message= "success";
			    					
			    		        }
			    	
			    }
			    
		
		}
		else
		{
		 
	
			String sql1="insert into feedback1 (ipaddress,comment,time) values (?,?,?)"; 
			PreparedStatement st3 = con.prepareStatement(sql1, Statement.RETURN_GENERATED_KEYS);
			//st3.setString(1, "192.168.50.88");
			st3.setString(1, ipAddress);
			st3.setString(2, comment2);
			Timestamp timestamp1 = new Timestamp(System.currentTimeMillis());
			st3.setTimestamp(3,timestamp1);
			int row1=st3.executeUpdate();
			int lastid = 0;
			 ResultSet rs2 = st3.getGeneratedKeys();
			  
			    if (rs2.next())
			    {
			        lastid=rs2.getInt(1);
			        System.out.println("id"+lastid);
			    }
			    
			    String sql3="insert into feedback_detail (questions,star_rating,feedno) values (?,?,?)"; 
				PreparedStatement stmt3 = con.prepareStatement(sql3, Statement.RETURN_GENERATED_KEYS);
				
			
				stmt3.setString(1, "How would you rate the search feature?");
				stmt3.setInt(2, star1);
				stmt3.setInt(3, lastid);
			
			    int row3=stmt3.executeUpdate();
			    String sql4="insert into feedback_detail (questions,star_rating,feedno) values (?,?,?)"; 
				PreparedStatement stmt4 = con.prepareStatement(sql4, Statement.RETURN_GENERATED_KEYS);
				
			
				stmt4.setString(1, "How would you rate the filter feature?");
				stmt4.setInt(2, star2);
				stmt4.setInt(3, lastid);
				int row4=stmt4.executeUpdate();
			    
		        String sql5="insert into feedback_detail (questions,star_rating,feedno) values (?,?,?)"; 
				PreparedStatement stmt5 = con.prepareStatement(sql5, Statement.RETURN_GENERATED_KEYS);
				stmt5.setString(1, "How would you rate the print feature?");
				stmt5.setInt(2, star3);
				stmt5.setInt(3, lastid);
				int row5=stmt5.executeUpdate();
				
				String sql6="insert into feedback_detail (questions,star_rating,feedno) values (?,?,?)"; 
				PreparedStatement stmt6 = con.prepareStatement(sql6, Statement.RETURN_GENERATED_KEYS);
				stmt6.setString(1, "How would you rate your overall satisfaction this application?");
				stmt6.setInt(2, star4);
				stmt6.setInt(3, lastid);
				int row6=stmt6.executeUpdate();
				
				
				
				if((row3>0 && row4>0)&& (row5>0 && row6>0))
				{
					message= "success";
				}
				
		}	
		
		}
				catch(Exception e)
				{ 
					message= "failed";
					System.out.println(e);
				}
			return message;				
		
			    
		}	    
			
		
		
		
		
		
	public String insertfeedback2(int star4,int star1,int star2,int star3,String comment2){
		String message = "";
		try{
	    
		DbOperation db=new DbOperation();
		Connection con=db.getConnectionwithdb();
		
		String sql="insert into feedback (question,starrating,feedback,time) values (?,?,?,?)"; 
		PreparedStatement stmt = con.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
		
	
		stmt.setString(1, "How would you rate your overall satisfaction this application?");
		stmt.setInt(2, star4);
		stmt.setString(3, comment2);
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		stmt.setTimestamp(4,timestamp);
	    int row=stmt.executeUpdate();
	    int risultato=0;
	    ResultSet rs = stmt.getGeneratedKeys();
	  
	    if (rs.next()){
	        risultato=rs.getInt(1);
	        System.out.println("id"+risultato);
	    }
	    
	    String sql1="insert into feedback_detail (questions,rating,feedbacksrno) values (?,?,?)"; 
		PreparedStatement stmt1 = con.prepareStatement(sql1, Statement.RETURN_GENERATED_KEYS);
		
	
		stmt1.setString(1, "How would you rate the search feature?");
		stmt1.setInt(2, star1);
		stmt1.setInt(3, risultato);
	
	    int row1=stmt1.executeUpdate();
	    String sql2="insert into feedback_detail (questions,rating,feedbacksrno) values (?,?,?)"; 
		PreparedStatement stmt2 = con.prepareStatement(sql2, Statement.RETURN_GENERATED_KEYS);
		stmt2.setString(1, "How would you rate the filter feature?");
		stmt2.setInt(2, star2);
		stmt2.setInt(3, risultato);
		int row2=stmt2.executeUpdate();
	    
        String sql3="insert into feedback_detail (questions,rating,feedbacksrno) values (?,?,?)"; 
		PreparedStatement stmt3 = con.prepareStatement(sql3, Statement.RETURN_GENERATED_KEYS);
		stmt3.setString(1, "How would you rate the print feature?");
		stmt3.setInt(2, star3);
		stmt3.setInt(3, risultato);
		int row3=stmt3.executeUpdate();
		    
		
		
		if((row>0 && row1>0)&& (row2>0 && row3>0))
		{
			message= "success";
		}
		else
		{
			message="failed";
		}
		
	  }
		catch(Exception e)
		{ 
			message= "failed";
			System.out.println(e);
	    } 
		return message;
	}
	
	//192.168.2.11
	
	public String InsertEnquiry(String username,String emailid,String mob,String sub,String enquiry){
		String message = "";
		try{
	    
		DbOperation db=new DbOperation();
		Connection con=db.getConnectionwithdb();
		
		String enqquery="insert into Enquiry (name,email,mobile,subject,message) values (?,?,?,?,?)"; 
		PreparedStatement stmt = con.prepareStatement(enqquery, Statement.RETURN_GENERATED_KEYS);
		
	
		stmt.setString(1,username );
		stmt.setString(2, emailid);
		stmt.setString(3, mob);
		stmt.setString(4, sub);
		stmt.setString(5, enquiry);
		

		//Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		//stmt.setTimestamp(4,timestamp);
	    int row1=stmt.executeUpdate();
	    
	
		if(row1>0)
		{
			message= "success";
		}
		else
		{
			message="failed";
		}
	
		
	  }
		catch(Exception e)
		{ 
			message= "failed";
			System.out.println(e);
	    } 
		return message;
		
	}
	

public String InsertContacts(String username,String emailid,String mob,String sub,String comments1){
	String message = "";
	try{
    
	DbOperation db=new DbOperation();
	Connection con=db.getConnectionwithdb();
	
	String cotactsquery1="insert into Contacts (name,email,phone,subject,message) values (?,?,?,?,?)"; 
	PreparedStatement stmts = con.prepareStatement(cotactsquery1, Statement.RETURN_GENERATED_KEYS);
	

	stmts.setString(1,username);
	stmts.setString(2, emailid);
	stmts.setString(3, mob);
	stmts.setString(4, sub);
	stmts.setString(5, comments1);
		
	//Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	//stmt.setTimestamp(4,timestamp);
    int rows=stmts.executeUpdate();
	if(rows>0)
	{
		message= "success";
		
	}
	else
	{
		message="db operation  failed";
		
	}
	
  }
	catch(Exception e)
	{ 
		message= "failed Ex";
		System.out.println(message);
		System.out.println(e);
    }
	return message; 
	
}

public String InsertCareer(String name, String email, String mobile, String currentcomp, String linked, String gitlab,
		String portfolio, String otherweb, FileInputStream fis, File file){
		
	String message = "";
	try{
    
	DbOperation db=new DbOperation();
	Connection con=db.getConnectionwithdb();
	
	String cotactsquery1="insert into career(name,email,number,currentcompany,linkedln,gitlab,portfolio,otherweb,resume) values (?,?,?,?,?,?,?,?,?)"; 
	PreparedStatement stmts = con.prepareStatement(cotactsquery1, Statement.RETURN_GENERATED_KEYS);
	

	stmts.setString(1, name);
	stmts.setString(2, email);
	stmts.setString(3, mobile);
	stmts.setString(4, currentcomp);
	stmts.setString(5, linked);
	stmts.setString(6, gitlab);
	stmts.setString(7, portfolio);
	stmts.setString(8, otherweb);
	stmts.setBinaryStream(9, fis, (int)(file.length()));
	
		
	//Timestamp timestamp = new Timestamp(System.currentTimeMillis());
	//stmt.setTimestamp(4,timestamp);
    int rows=stmts.executeUpdate();
	if(rows>0)
	{
		message= "success";
		
	}
	else
	{
		message="db operation  failed";
		
	}
	
  }
	catch(Exception e)
	{ 
		message= "failed Ex";
		System.out.println(message);
		System.out.println(e);
    }
	return message; 
	
}


public void getPDFData() {
    
 	DbOperation db=new DbOperation();
	Connection con=db.getConnectionwithdb();
	
	
    byte[] fileBytes;
    String query;
    try {
        query = 
          "select resume from career where careerid=12";
        Statement state = con.createStatement();
        ResultSet rs = state.executeQuery(query);
        if (rs.next()) {
            fileBytes = rs.getBytes(1);
            OutputStream targetFile=  new FileOutputStream(
                    "d://webmaster/kk.pdf");
            targetFile.write(fileBytes);
            targetFile.close();
        }
         
    } catch (Exception e) {
        e.printStackTrace();
    }
}





}
